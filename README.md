# Yeelight

[![Add To Installation](https://marketplace.signalrgb.com/resources/add-extension-256.png 'Add to My SignalRGB Installation')](signalrgb://addon/install?url=https://gitlab.com/signalrgb/yeelight)

## Getting started
This is a simple SignalRGB extension for controlling UDP Compatible Yeelight Devices.

## Known Issues
- Currently we cannot fetch the cube topology or led counts.
- Single zone devices cannot do the color black.
- TCP devices are not controllable.

## Installation
Click the button above and allow signalrgb to install this extension when prompted.

## Support
Feel free to open issues here, or submit PRs.
